import 'package:flutter/material.dart';
import 'package:novlencetask/OTP.dart';
import 'package:firebase_auth/firebase_auth.dart';

class login extends StatefulWidget
{
  _loginState createState() => _loginState();
}


class _loginState extends State<login> {
  String input='+91';
  String _input;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    String dropdownValue= 'assets/India.png';



    buttonpressed(String button)
    {
      _input=button;


      setState(() {

        input+=_input;
      });

    }

    Widget customappbar()
    {
      return Container(
        height: 150.0,
        width: 500.0,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(70.0))
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 25.0
              ),
              child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  color: Colors.grey,
                  onPressed: (){ Navigator.of(context).pushReplacementNamed('splash');}),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 60.0
              ),
              child: Text('Phone number',style:
                TextStyle(
                  color: Colors.red[300],
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold
                ),),
            )

          ],
        ),
      );
    }

    Widget buttons(int num)
    {
      String n= num.toString();
      return Container(
        width: 80.0,
        height: 75.0,
        child: RaisedButton(
          onPressed: (){
            buttonpressed(n);
          },
          shape: CircleBorder(),
          elevation: 10.0,
          color: Colors.white,
          child: Text(n,
            style: TextStyle(
                fontSize: 30.0,
                color: Colors.red[300],
                fontWeight: FontWeight.w300
            ),
          ),
        ),
      );
    }
    return Scaffold(
      backgroundColor: Colors.red.shade400,
      body: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 175.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    DropdownButton<String>(
                      value: dropdownValue,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue = newValue;
                        });
                      },
                      items: <String>['assets/India.png', 'assets/butterfly.png',]
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Image.asset(value),
                        );
                      }).toList(),
                    ),

                    Text(input,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 30.0
                    ),
                    )
                  ],
                ),

                Padding(
                  padding: const EdgeInsets.only(
                      left: 30.0,
                      right: 30.0

                  ),
                  child: Divider(
                    color: Colors.white,

                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 50.0,
                      bottom: 50.0
                    ),
                    child: Container(
                      width: 375.0,
                      height: 50.0,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(
                            builder: (BuildContext context) => otp(input),
                          ));
                        },
                        elevation: 10.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30.0))
                        ),
                        color: Colors.white,
                        child: Text('Verify',
                          style: TextStyle(
                              fontSize: 30.0,
                              color: Colors.red[300],
                              fontWeight: FontWeight.w300
                          ),
                        ),
                      ),
                    ),
                  ),
                )

                ,Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start
                  ,children: <Widget>[
                    Column(
                      children: <Widget>[
                        buttons(1),
                        SizedBox(height: 20.0),
                        buttons(4),
                        SizedBox(height: 20.0),
                        buttons(7),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        buttons(2),
                        SizedBox(height: 20.0),
                        buttons(5),
                        SizedBox(height: 20.0),
                        buttons(8),
                        SizedBox(height: 20.0),
                        buttons(0),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        buttons(3),
                        SizedBox(height: 20.0),
                        buttons(6),
                        SizedBox(height: 20.0),
                        buttons(9),
                        SizedBox(height: 20.0),


                        Container(
                          width: 80.0,
                          height: 75.0,
                          child: RaisedButton(
                            onPressed: (){
                              setState(() {
                                int l=input.length-1;
                                input=input.substring(0,l);
                              });

                                  print(input);
                            },
                            shape: CircleBorder(),
                            elevation: 10.0,
                            color: Colors.white,
                            child: Icon(
                              Icons.clear,
                              color: Colors.red[300],
                            )
                          ),
                        )
                      ],
                    )
                  ],
                )

              ],
            ),
          )
          ,customappbar()

        ],
      ),
    );
  }
}
