import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class otp extends StatefulWidget
{
  String number;
  otp(this.number){
  }
  _otpState createState() => _otpState(number);
}


class _otpState extends State<otp> {
  TextEditingController _smsCodeController = TextEditingController();
  String verificationId;
  FirebaseAuth _auth = FirebaseAuth.instance;

  /// Sends the code to the specified phone number.
  Future<void> _sendCodeToPhoneNumber() async {
    final PhoneVerificationCompleted verificationCompleted = (FirebaseUser user) {
      setState(() {
        print('Inside _sendCodeToPhoneNumber: signInWithPhoneNumber auto succeeded: $user');
      });
    };

    final PhoneVerificationFailed verificationFailed = (AuthException authException) {
      setState(() {
        print('Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');}
      );
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
      print("code sent to " + phoneNo);
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      this.verificationId = verificationId;
      print("time out");
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNo,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Future<String> _SignInWithPhoneNumber(String smsCode) async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: verificationId,
      smsCode: smsCode,
    );
    final FirebaseUser user = await _auth.signInWithCredential(credential);
    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);
    
    _smsCodeController.text = '';
    return 'signInWithPhoneNumber succeeded: $user';
  }


  final FocusNode _firstInputFocusNode = new FocusNode();
  final FocusNode _secondInputFocusNode = new FocusNode();
  final FocusNode _thirdInputFocusNode = new FocusNode();
  final FocusNode _fourthInputFocusNode = new FocusNode();
  final FocusNode _fifthInputFocusNode = new FocusNode();
  final FocusNode _sixthInputFocusNode = new FocusNode();

  String phoneNo;


  _otpState(this.phoneNo){
    print(phoneNo);
    _sendCodeToPhoneNumber();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build


    Widget buttons(int num)
    {
      String n= num.toString();
      return Container(
        width: 80.0,
        height: 75.0,
        child: RawMaterialButton(
          onPressed: (){print(n);
          },
          //color: Colors.blue,
          child: Text(n,
            style: TextStyle(
                fontSize: 30.0,
                color: Colors.black,
                fontWeight: FontWeight.w300
            ),
          ),
        ),
      );
    }

    Widget customappbar()
    {
      return Container(
        height: 150.0,
        width: 500.0,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(70.0))
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 25.0
                  ),
                  child: IconButton(
                      icon: Icon(Icons.arrow_back),
                      color: Colors.white10,
                      onPressed:(){ Navigator.of(context).pushReplacementNamed('login');}),
                ),

                Padding(
                  padding: const EdgeInsets.only(
                      top: 25.0,
                  left: 300.0),
                  child: IconButton(
                      icon: Icon(Icons.filter_list),
                      onPressed: null),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 60.0
              ),
              child: Text('Verification Code',style:
              TextStyle(
                  color: Colors.black,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold
              ),),
            )

          ],
        ),
      );
    }

    void _onpressed(int num)
    {


    }

    return Scaffold(
      backgroundColor: Colors.grey,
      body: Column(
        children: <Widget>[
          customappbar(),

          /*Padding(
            padding: const EdgeInsets.only(top: 30.0),

            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,

              children: <Widget>[

                Container(
                  height: 80,
                  width: 50,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0))

                  ),
                  child: TextField(
                    decoration:null,
                    focusNode: _firstInputFocusNode,
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () =>
                        FocusScope.of(context).requestFocus(_secondInputFocusNode),
                  ),
                ),

                SizedBox(width: 10),

                Container(
                  height: 80,
                  width: 50,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0))

                  ),
                  child: TextField(
                    decoration: null,
                    focusNode: _secondInputFocusNode,
                    textInputAction: TextInputAction.next,
                    onEditingComplete:() =>
                        FocusScope.of(context).requestFocus(_thirdInputFocusNode) ,
                  ),
                ),

                SizedBox(width: 10),

                Container(
                  height: 80,
                  width: 50,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0))

                  ),
                  child: TextField(
                    decoration: null,
                    focusNode: _thirdInputFocusNode,
                    textInputAction: TextInputAction.next,
                    onEditingComplete:() =>
                        FocusScope.of(context).requestFocus(_fourthInputFocusNode),
                  ),
                ),

                SizedBox(width: 10),

                Container(
                  height: 80,
                  width: 50,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0))

                  ),
                  child: TextField(
                    decoration: null,
                    focusNode: _fourthInputFocusNode,
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () =>
                        FocusScope.of(context).requestFocus(_fifthInputFocusNode),
                  ),
                ),

                SizedBox(width: 10),

                Container(
                  height: 80,
                  width: 50,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0))

                  ),
                  child: TextField(
                    decoration:null,
                    focusNode: _fifthInputFocusNode,
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () =>
                        FocusScope.of(context).requestFocus(_sixthInputFocusNode),
                  ),
                ),

                SizedBox(width: 10),

                Container(
                  height: 80,
                  width: 50,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0))

                  ),
                  child: TextField(
                    decoration: null,
                    focusNode: _sixthInputFocusNode,
                    textInputAction: TextInputAction.done,
                  ),
                ),


              ],
            ),
          ),*/

          TextField(controller: _smsCodeController,),


          FlatButton(
              onPressed: (){},
              child: Text('Resend code',
              style: TextStyle(
                color: Colors.red[300]
              )
              )
          ),

          Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                height: 100.0,
                decoration: BoxDecoration(
                  color: Colors.red[300],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(75.0),
                  )
                )
          ),
                 Padding(
                   padding: const EdgeInsets.only(
                       top: 10.0,
                       bottom: 50.0,
                   ),
                   child: Center(
                     child: Container(
                       width: 80.0,
                       height: 40.0,
                       child: RaisedButton(
                         onPressed: () => _SignInWithPhoneNumber(_smsCodeController.text),
                         elevation: 10.0,
                         shape: RoundedRectangleBorder(
                             borderRadius: BorderRadius.all(Radius.circular(75.0))
                         ),
                         color: Colors.white,
                         child: Text('Next',
                           style: TextStyle(
                               fontSize: 20.0,
                               color: Colors.red[300],
                               fontWeight: FontWeight.w300
                           ),
                         ),
                       ),
                     ),
                   ),
                 ),

              Positioned(
                top: 60,
                child: Container(
                  height: 450.0,
                  width: 420.0,
                  decoration:BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(60.0)
                    )
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start
                    ,children: <Widget>[
                    Column(
                      children: <Widget>[
                        buttons(1),
                        SizedBox(height: 20.0),
                        buttons(4),
                        SizedBox(height: 20.0),
                        buttons(7),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        buttons(2),
                        SizedBox(height: 20.0),
                        buttons(5),
                        SizedBox(height: 20.0),
                        buttons(8),
                        SizedBox(height: 20.0),
                        buttons(0),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        buttons(3),
                        SizedBox(height: 20.0),
                        buttons(6),
                        SizedBox(height: 20.0),
                        buttons(9),
                        SizedBox(height: 20.0),
                        Container(
                          width: 80.0,
                          height: 75.0,
                          child: RawMaterialButton(
                            onPressed: (){
                            },
                            //color: Colors.blue,
                            child: Icon(Icons.clear,
                            color: Colors.black,)
                          ),
                        )
                      ],
                    )
                  ],
                  ),
                ),
              )
            ],
          )

        ],
      ),
    );
  }

}