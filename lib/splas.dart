import 'package:flutter/material.dart';

class splas extends StatefulWidget
{
  _splasState createState() => _splasState();
}

class _splasState extends State<splas>
{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
         Image.asset('assets/beach2.jpg',
           height: 600.0,
           fit: BoxFit.fill,),
          Positioned(
            left: 40.0,
            top: 450.0,
            child: Container(
              height: 225,
              width: 400,
            decoration:
            BoxDecoration(
              color: Colors.white,
                boxShadow: [BoxShadow(
                  color: Colors.black,
                  blurRadius: 10.0
                ),
                ],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                    bottomLeft: Radius.circular(60.0)
                )
            ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 40.0,
                      left: 60.0
                    ),
                    child: Text('Travellers,',
                    style: TextStyle(
                      color: Colors.red[300],
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300
                    ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 60.0,
                      top: 10.0
                    ),
                    child: Text('Welcome to',
                    style: TextStyle(
                      color: Colors.red[300],
                      fontSize: 40.0,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5
                    ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 275.0),
                    child: Image.asset('assets/butterfly.png',
                      scale: 2.5,),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 650.0,
            left: 225.0,
            child: RawMaterialButton(
              onPressed: (){ Navigator.of(context).pushReplacementNamed('login');},
              child: Container(
                width: 300.0,
                height: 75.0,
                decoration: BoxDecoration(
                  color: Colors.red[300],
                  borderRadius: BorderRadius.only(
                    bottomLeft:Radius.circular(60.0),
                    topLeft: Radius.circular(60.0)
                  )
                ),
                child: Row(
                children: <Widget>[
                Padding(
                    padding: const EdgeInsets.only(
                    left: 10.0,
                    right: 5.0),
                child: Text('Get Started',style:
                TextStyle(fontSize: 25.0,
                    color: Colors.white
                ),
                ),
              ),
              Icon(Icons.arrow_forward,
                color: Colors.white,)
              ],
          ),
              ),
            ),
          ),
          Positioned(
            top: 175.0,
              left: 50.0,
              child: Image.asset('assets/logo.png',
              scale: 1.5,)
          ),
          Positioned(
            left: 340.0,
            top: 50.0,
            child: Row(
              children: <Widget>[
                RawMaterialButton(
                  onPressed: (){ Navigator.of(context).pushReplacementNamed('login');}
                  ,child: Text('SKIP',style: TextStyle(
                    color: Colors.grey,
                    fontSize: 20.0
                  ),
                  ),
                )
              ],
            ),
          )

        ],
      ),
    );
  }
}